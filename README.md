# README #

### What is mcqshuffle ###
It is an incredibly simple shuffler of MCQs which are stored in spreadsheets. 

### How do I get set up? ###
You need python2.7, xlrd, and xlwt. That's pretty much it.

###Format of the spreadsheet####
The questions have to be stored on a sheet called Sheet1. Each row should contain the question and the choices. A sample file called test.xls is provided. 

###Usage###
$ python mcqshuffle <filename> <number_copies>