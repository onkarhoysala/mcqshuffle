__author__ = 'onkar'
import xlrd
import xlwt
from random import shuffle
import os
import sys


class MCQ:
    question = ""
    answers = []

    def __init__(self):
        self.question = ""
        self.answers = []

    def randomize(self):
        if len(self.answers) != 0:
            shuffle(self.answers)


if __name__ == "__main__":
    mcqs = []

    if len(sys.argv) != 3:
        filename = 'test.xls'
        ncopies = 5
    else:
        filename = sys.argv[1]
        ncopies = int(sys.argv[2])

    foldername = filename.split(".xls")[0]
    workbook = xlrd.open_workbook(filename)
    worksheet = workbook.sheet_by_name('Sheet1')
    numrows = worksheet.nrows
    numcols = worksheet.ncols
    for i in range(0, numrows):
        temp = MCQ()
        temp.question = worksheet.cell_value(i, 0)
        for j in range(1, numcols):
            if worksheet.cell_value(i, j) != "":
                temp.answers.append(worksheet.cell_value(i, j))
        mcqs.append(temp)

    print "Read", str(len(mcqs)), "questions. Shuffling them and creating", str(ncopies), "copies."

    if not os.path.isdir(foldername):
        os.mkdir(foldername)

    # i is the number of outputs
    for i in range(1, ncopies + 1):
        shuffle(mcqs)
        w = xlwt.Workbook()
        ws = w.add_sheet('Sheet1')
        #qno counts the row/question
        qno = 0
        for q in mcqs:
            q.randomize()
            ws.write(qno, 0, q.question)
            #choiceno counts the choices. j,k give the cell
            choiceno = 1
            for choice in q.answers:
                ws.write(qno, choiceno, choice)
                choiceno += 1
            qno += 1
        w.save(foldername + "/" + foldername + "_" + str(i) + ".xls")

